import { Carousel } from "antd";
import React from "react";
import { useState, useEffect } from "react";
import SlideItem from "./SlideItem";
import { movieServ } from "../../Services/movieService";
import SearchMovie from "./SearchMovie";

function Carousels() {
  const [listBanner, setListBanner] = useState([]);
  useEffect(() => {
    let fetchBanner = async () => {
      try {
        let response = await movieServ.getListBanner();
        setListBanner(response.data.content);
      } catch (err) {
        console.log(err);
      }
    };
    fetchBanner();
  }, []);
  let renderSlideCarousel = () => {
    return listBanner.map((item, index) => {
      return <SlideItem index={index} key={index} item={item} />;
    });
  };
  return (
    <div style={{ height: "100vh" }} className="relative w-full">
      <Carousel
        className="absolute top-0 left-0 mt-50"
        autoplay
        effect="fade"
        dotPosition="bottom">
        {renderSlideCarousel()}
      </Carousel>
      <SearchMovie />
    </div>
  );
}

export default Carousels;
