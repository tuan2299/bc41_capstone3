import React from "react";
import { listTrailer } from "./dataTrailer";
import "animate.css";
import { useDispatch } from "react-redux";
import { handleMoModal } from "../../Toolkits/modalSlice";
function SlideItem({ item, index }) {
  let dispatch = useDispatch();
  const contentStyle = {
    width: "100%",
    height: "70vh",
    color: "#f6fff8",
    textAlign: "center",
  };
  let handleMo = (url) => {
    dispatch(handleMoModal(url));
  };
  return (
    <div className="relative" style={contentStyle}>
      <img
        className="absolute top-0 left-0 w-full h-full"
        src={item.hinhAnh}
        alt="banner"
      />
      <div className="absolute top-0 left-0 flex items-center justify-center w-full h-full slide__movie">
        <i
          onClick={() => handleMo(listTrailer[index])}
          className="w-20 h-20 text-5xl transition border fa fa-play-circle hover:scale-125 button__play"
          style={{ borderRadius: "50%", cursor: "pointer" }}
          aria-hidden="true"></i>
      </div>
    </div>
  );
}

export default SlideItem;
