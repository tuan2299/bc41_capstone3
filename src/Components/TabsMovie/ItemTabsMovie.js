import moment from "moment/moment";
import React from "react";

export default function ItemTabsMovie({ phim }) {
  return (
    <div className="flex items-start movieShowtime">
      <img className="" src={phim.hinhAnh} alt="" />
      <div className="detailShowtime">
        <h5 className="flex items-center justify-start">
          <span className="mr-3 movieTag">C18</span>
          {phim.tenPhim}
        </h5>
        <div>
          {phim.lstLichChieuTheoPhim.slice(0, 6).map((item) => {
            return (
              <a className="showtime" href="#" key={item.maLichChieu}>
                <span>
                  {moment(item.ngayChieuGioChieu).format("DD-mm-yyyy ")}
                </span>
                ~<span> {moment(item.ngayChieuGioChieu).format("hh:mm")}</span>
              </a>
            );
          })}
        </div>
      </div>
    </div>
  );
}
