import React from "react";

import TabsMovie from "./TabsMovie";

export default function TabsMovieLayout() {
  return (
    <div>
      <TabsMovie />
    </div>
  );
}
