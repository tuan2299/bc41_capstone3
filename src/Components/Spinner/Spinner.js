import React from "react";
import { useSelector } from "react-redux";
import { HashLoader } from "react-spinners";

// sử dụng thư viện react-spinners để tạo hiệu ứng loading cho website

function Spinner() {
  let { isLoading } = useSelector((state) => state.spinnerSlice);
  if (isLoading) {
    return (
      <div
        style={{ background: "#f6fff8" }}
        className="fixed top-0 left-0 z-50 flex items-center justify-center w-screen h-screen">
        <HashLoader color="#36d7b7" />
      </div>
    );
  } else {
    return <></>;
  }
}

export default Spinner;
