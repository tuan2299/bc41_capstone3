import React, { useState, useEffect } from "react";
import { movieServ } from "../../Services/movieService";
import MovieSlider from "./MovieSlider";
import queryString from "query-string";
function MovieList() {
  const [movieList, setMovieList] = useState([]);
  useEffect(() => {
    const query = queryString.parse(window.location.search);
    let value = query.tenPhim;
    if (value) {
      let fetchSearchMovie = async () => {
        try {
          let response = await movieServ.searchMovie(value);
          setMovieList(response.data.content);
        } catch (err) {
          console.log(err);
        }
      };
      fetchSearchMovie();
    } else {
      let fetchMovieList = async () => {
        try {
          let response = await movieServ.getListMovie();
          setMovieList(response.data.content);
        } catch (err) {
          console.log(err);
        }
      };
      fetchMovieList();
    }
    window.history.replaceState(null, null, "/");
  }, []);
  return (
    <div
      id="lich__chieu"
      className="mt-24 lg:w-3/5 lg:mx-auto md:w-11 md:mx-auto sm:mt-24">
      <MovieSlider movieList={movieList} />
    </div>
  );
}

export default MovieList;
