import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { Card, Button } from "antd";
import "animate.css";
import { useDispatch } from "react-redux";
import { handleMoModal } from "../../Toolkits/modalSlice";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
function MovieSlider({ movieList }) {
  let dispatch = useDispatch();
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  };
  let handleMo = (url) => {
    dispatch(handleMoModal(url));
  };
  return (
    <Slider
      {...settings}
      className="h-full overflow-y-visible sm:my-5 lg:my-20 overflow-x-clip sm:overflow-x-clip sm:overflow-hidden md:overflow-visible lg:overflow-visible">
      {movieList.map((movie, index) => {
        return (
          <div key={index} className="z-50 w-full h-full">
            <Card
              key={index}
              hoverable
              cover={
                <div className="relative w-full h-80 lg:h-80 md:h-72">
                  <img
                    className="absolute top-0 left-0 object-cover w-full h-80 lg:h-80 md:h-80"
                    alt="example"
                    src={movie.hinhAnh}
                  />
                  <i
                    onClick={() => {
                      handleMo(movie.trailer);
                    }}
                    className="absolute w-20 h-20 text-4xl transition -translate-x-1/2 -translate-y-1/2 border text-gray-50 fa fa-play-circle hover:scale-125 button__play top-1/2 left-1/2 "
                    style={{
                      borderRadius: "50%",
                      cursor: "pointer",
                      lineHeight: "70px",
                    }}
                    aria-hidden="true"></i>
                </div>
              }
              className="mr-5 overflow-hidden overflow-y-visible text-center lg:overflow-x-hidden md:overflow-x-hidden"
              style={{ height: "500px", width: "95%", margin: "0 auto" }}>
              <Meta title={movie.tenPhim} description={movie.moTa} />
              <NavLink to={`/detail/${movie.maPhim}`}>
                <Button className="button__booking " type="primary" danger>
                  Mua vé
                </Button>
              </NavLink>
            </Card>
          </div>
        );
      })}
    </Slider>
  );
}

export default MovieSlider;
