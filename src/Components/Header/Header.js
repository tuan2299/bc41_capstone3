import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";
import "bootstrap/dist/css/bootstrap.min.css";
import HeaderUserInfo from "./HeaderUserInfo";

function Headers() {
  return (
    <>
      {["lg"].map((moRong) => (
        <Navbar
          fixed="top"
          key={moRong}
          moRong={moRong}
          style={{ background: "#f6fff8" }}
          className="z-50 h-20 mb-2 font-bold shadow-md">
          <Container fluid>
            <Navbar.Brand href="/">
              <img
                src="https://demo1.cybersoft.edu.vn/logo.png"
                className="w-48"
                alt="logo"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-${moRong}`} />
            <Navbar.Offcanvas
              id={`offcanvasNavbar-expand-${moRong}`}
              aria-labelledby={`offcanvasNavbarLabel-expand-${moRong}`}
              placement="end">
              <Offcanvas.Header closeButton>
                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${moRong}`}>
                  CyberFlix
                </Offcanvas.Title>
              </Offcanvas.Header>
              <Offcanvas.Body className="items-center lg:space-x-10">
                <Nav className="justify-content-end flex-grow-1 pe-3">
                  <Nav.Link
                    className="pb-6 mr-5 border-b-2 border-gray-400 nav__link"
                    href="#lich__chieu">
                    Lịch Chiếu
                  </Nav.Link>
                  <Nav.Link
                    className="pb-6 mr-5 border-b-2 border-gray-400 nav__link"
                    href="#tabsMovie">
                    Cụm Rạp
                  </Nav.Link>
                  <Nav.Link
                    className="pb-6 mr-5 border-b-2 border-gray-400 nav__link"
                    href="#tabsNews">
                    Tin Tức
                  </Nav.Link>
                </Nav>
                <HeaderUserInfo />
              </Offcanvas.Body>
            </Navbar.Offcanvas>
          </Container>
        </Navbar>
      ))}
    </>
  );
}

export default Headers;
