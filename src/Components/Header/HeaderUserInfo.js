import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../Services/localService";

function HeaderUserInfo() {
  let { userInfo } = useSelector((state) => state.userSlice);
  let handleDangXuat = () => {
    localUserServ.remove();
    window.location.href = "/";
  };
  if (userInfo) {
    return (
      <div className="flex items-center sm:mt-5 lg:mt-0">
        <NavLink style={{ textDecoration: "none" }}>
          <span className="pr-3 text-gray-500 border-r-2 hover:text-purple-500">
            {userInfo.hoTen}
          </span>
        </NavLink>
        <NavLink onClick={handleDangXuat} style={{ textDecoration: "none" }}>
          <span className="flex items-center ml-3 text-gray-500 hover:text-purple-500">
            <i class="fas fa-sign-out-alt fa-lg fa-fw"></i>
            <small className="ml-2 text-sm">Đăng xuất</small>
          </span>
        </NavLink>
      </div>
    );
  } else {
    return (
      <div className="flex items-center sm:mt-5 lg:mt-0">
        <NavLink style={{ textDecoration: "none" }} to="/login">
          <span className="flex items-center pr-3 text-gray-500 border-r-2 hover:text-purple-500">
            <i className="text-3xl fa fa-user-circle" aria-hidden="true"></i>
            <small className="ml-2 text-sm">Đăng nhập</small>
          </span>
        </NavLink>
        <NavLink style={{ textDecoration: "none" }} to="/register">
          <span className="flex items-center ml-3 text-gray-500 hover:text-purple-500">
            <i className="text-3xl fa fa-user-circle" aria-hidden="true"></i>
            <small className="ml-2 text-sm">Đăng ký</small>
          </span>
        </NavLink>
      </div>
    );
  }
}

export default HeaderUserInfo;
