import React from "react";
import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { movieServ } from "../../Services/movieService";
import moment from "moment";
import { Button, Progress, Rate, Tabs } from "antd";
import { useDispatch } from "react-redux";
import { handleMoModal } from "../../Toolkits/modalSlice";
const onChange = (key) => {};
function DetailMovie() {
  let dispatch = useDispatch();
  let { id } = useParams();
  const [movie, setMovie] = useState({});
  const [theaterMovie, setTheaterMovie] = useState([]);
  useEffect(() => {
    let fetchDetailedMovie = async () => {
      try {
        let response1 = await movieServ.getDetailedMovie(id);
        let response2 = await movieServ.getTheaterDetailedMovie(id);
        setTheaterMovie(response2.data.content.heThongRapChieu);
        setMovie(response1.data.content);
      } catch (err) {
        console.log(err);
      }
    };
    fetchDetailedMovie();
  }, []);
  let handleMo = (url) => {
    dispatch(handleMoModal(url));
  };
  let renderLichChieuPhim = () => {
    return theaterMovie.map((heThongRap, index) => {
      return {
        key: heThongRap.maHeThongRap,
        label: (
          <div className="logoTheater">
            <img src={heThongRap.logo} alt="" className="object-contain w-10" />
          </div>
        ),
        children: heThongRap.cumRapChieu.map((cumRap, index) => {
          return (
            <div key={index}>
              <h2 className="text-lg text-blue-500">{cumRap.tenCumRap}</h2>
              <div className="grid grid-cols-4 gap-x-5">
                {cumRap.lichChieuPhim.map((date) => {
                  return (
                    <a
                      style={{ textDecoration: "none" }}
                      className="p-2 border rounded showtime hover:font-bold"
                      href="#"
                      key={date.maLichChieu}>
                      <span className="text-gray-500">
                        {moment(date.ngayChieuGioChieu).format("DD-mm-yyyy ")}
                      </span>
                      ~
                      <span className="text-red-500">
                        {" "}
                        {moment(date.ngayChieuGioChieu).format("hh:mm")}
                      </span>
                    </a>
                  );
                })}
              </div>
            </div>
          );
        }),
      };
    });
  };
  return (
    <div className="flex flex-wrap justify-between w-full py-40 mx-auto mb-20 lg:w-3/5 md:w-11/12 sm:w-full">
      <div className="flex title__movie">
        <div className="w-1/3 h-64 mr-5 overflow-hidden rounded">
          <img className="w-full h-full" src={movie.hinhAnh} />
        </div>
        <div className="space-y-5 lg:w-2/3">
          <h2 className="text-2xl text-red-500">{movie.tenPhim}</h2>
          <span className="block">{moment(movie.ngayKhoiChieu)}</span>
          <Button type="primary" danger>
            Mua vé
          </Button>
          <i
            onClick={() => {
              handleMo(movie.trailer);
            }}
            className="block text-5xl text-black fa fa-play-circle"
            style={{ cursor: "pointer" }}
            aria-hidden="true"></i>
        </div>
      </div>
      <div className="rating__movie">
        <Progress
          size={150}
          type="circle"
          format={(percent) => `${percent / 10} Đ`}
          percent={movie.danhGia * 100}
          strokeColor={{ "0%": "#0077b6", "100%": "#06d6a0" }}
        />
        <br />
        <Rate disabled defaultValue={5} />
      </div>
      <div className="flex-grow-0 py-10 tab__movie basis-full">
        <Tabs
          style={{ width: 760 }}
          tabPosition="left"
          items={renderLichChieuPhim()}
          defaultActiveKey="1"
          onChange={onChange}
        />
      </div>
    </div>
  );
}

export default DetailMovie;
