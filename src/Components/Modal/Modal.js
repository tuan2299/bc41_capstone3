import React from "react";
import ReactPlayer from "react-player";
import Modal from "react-modal";
import "animate.css";
import { useDispatch, useSelector } from "react-redux";
import { handleTatModal } from "../../Toolkits/modalSlice";

Modal.setAppElement("#root");
function ModalPlayer() {
  let dispatch = useDispatch();
  let handleTat = () => {
    dispatch(handleTatModal());
  };

  let { url, isOpen } = useSelector((state) => state.modalSlice);
  return (
    <Modal
      style={{ overlay: { zIndex: 20 }, content: { background: "#cce3de" } }}
      className="w-screen h-screen"
      isOpen={isOpen}
      onRequestClose={handleTat}>
      <div
        className="z-50 flex items-center justify-center w-full h-full cursor-pointer animate__animated animate__backInDown"
        onClick={handleTat}></div>
      <button
        className="absolute z-50 text-5xl text-black top-24 right-5"
        onClick={handleTat}>
        X
      </button>
    </Modal>
  );
}

export default ModalPlayer;
