import React from "react";
import { Modal } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { handleSuccess } from "../../Toolkits/formSuccessSlice";
import { CheckCircleOutlined } from "@ant-design/icons";
function SuccessForm() {
  let dispatch = useDispatch();
  let { isModalVisible } = useSelector((state) => state.formSuccessSlice);
  let handleSuccessModal = () => {
    dispatch(handleSuccess());
  };
  return (
    <div className="flex items-center justify-center">
      {/* Trang đăng ký */}
      <Modal
        className="text-center"
        open={isModalVisible}
        onOk={handleSuccessModal}
        onCancel={handleSuccessModal}>
        <div className="space-y-5 success-message">
          <CheckCircleOutlined
            style={{ fontSize: "150px", color: "#a4c3b2" }}
          />
          <p className="text-2xl font-bold">Đăng ký thành công.</p>
          <small style={{ color: "#6b9080" }} className="text-sm font-bold">
            Chuẩn bị chuyển sang trang đăng nhập...
          </small>
        </div>
      </Modal>
    </div>
  );
}

export default SuccessForm;
