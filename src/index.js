import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./Toolkits/userSlice";
import spinnerSlice from "./Toolkits/spinnerSlice";
import formSuccessSlice from "./Toolkits/formSuccessSlice";
import modalSlice from "./Toolkits/modalSlice";

const root = ReactDOM.createRoot(document.getElementById("root"));
const store = configureStore({
  reducer: {
    // bên trong object reducer của store sẽ liệt kê các slice đã tạo
    userSlice: userSlice,
    spinnerSlice: spinnerSlice,
    formSuccessSlice: formSuccessSlice,
    modalSlice: modalSlice,
  },
});
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
