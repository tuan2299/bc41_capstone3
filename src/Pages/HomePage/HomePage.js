import React from "react";
import Carousels from "../../Components/Carousel/Carousel";
import TabsMovieLayout from "../../Components/TabsMovie/TabsMovieLayout";
import MovieList from "../../Components/MovieList/MovieList";
import ModalPlayer from "../../Components/Modal/Modal";

function HomePage() {
  return (
    <div>
      <ModalPlayer />
      <Carousels />
      <MovieList />
      <TabsMovieLayout />
    </div>
  );
}

export default HomePage;
