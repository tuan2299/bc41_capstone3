import { Button, Form, Input, message } from "antd";
import { userServ } from "../../Services/userService";
import { NavLink, useNavigate } from "react-router-dom";
import {
  validateEmail,
  validateEqualMk,
  validateHt,
  validateMk,
  validatePhoneVietNam,
  validateTk,
} from "./ValidationForm";
import { useDispatch } from "react-redux";
import {
  closeFormSuccess,
  handleSuccess,
  registerSuccess,
} from "../../Toolkits/formSuccessSlice";
import React from "react";
import SuccessForm from "../../Components/SuccessForm/SuccessForm";
import { localUserServ } from "../../Services/localService";

function RegisterPage() {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = async (values) => {
    let cloneValues = { ...values, maNhom: "GP09" };
    let { taiKhoan, matKhau, matKhauNhapLai, hoTen, email, soDt } = cloneValues;
    let fetchRegisterUser = async () => {
      let isValid =
        validateTk(taiKhoan) &&
        validateMk(matKhau) &&
        validateEqualMk(matKhau, matKhauNhapLai) &&
        validateHt(hoTen) &&
        validateEmail(email) &&
        validatePhoneVietNam(soDt);
      if (isValid) {
        try {
          let response = await userServ.registerUser(values);
          console.log(response);
          dispatch(registerSuccess());
          localUserServ.set(response.data.content);
          // * sau 2s sẽ tắt thông báo
          setTimeout(() => {
            dispatch(closeFormSuccess());
          }, 2000);
          // * sau 3s sẽ chuyển tới trang đăng nhập
          setTimeout(() => {
            navigate("/login");
          }, 3000);
        } catch (err) {
          console.log(err);
          message.error(err.response.data.content);
        }
      }
    };
    fetchRegisterUser();
  };
  const onFinishFailed = (errInfo) => {
    console.log("Failed:", errInfo);
  };
  return (
    <>
      <SuccessForm />
      <Form
        name="basic"
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        className="sm:w-2/3 md:w-3/5 lg:w-1/3"
        style={{
          background: "white",
          borderRadius: "20px",
          padding: "15px 50px",
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="vertical">
        <div className="w-full mb-10 space-y-5 text-center">
          <i
            style={{ lineHeight: "45px", borderRadius: "50%" }}
            className="w-12 h-12 bg-blue-500 fas fa-lock fa-lg fa-fw"></i>
          <h2 className="text-3xl font-bold text-gray-700 ">Đăng ký</h2>
        </div>
        <Form.Item
          label="Tên Tài khoản"
          name="taiKhoan"
          rules={[
            {
              required: true,
              message: "Nhập tài khoản",
            },
          ]}>
          <Input
            className="py-3 text-gray-800 placeholder:font-bold hover:border-blue-600"
            placeholder="nhập tài khoản ..."
          />
        </Form.Item>
        <Form.Item
          name="matKhau"
          label="Nhập Mật khẩu"
          rules={[
            {
              required: true,
              message: "Nhập mật khẩu",
            },
          ]}>
          <Input.Password
            className="py-3 text-gray-800 input__password placeholder:font-bold hover:border-blue-600"
            placeholder="nhập mật khẩu ..."
          />
        </Form.Item>
        <Form.Item
          name="matKhauNhapLai"
          label="Nhập lại Mật khẩu"
          rules={[
            {
              required: true,
              message: "Nhập lại mật khẩu",
            },
          ]}>
          <Input.Password
            className="py-3 text-gray-800 input__password placeholder:font-bold hover:border-blue-600"
            placeholder="nhập lại mật khẩu ..."
          />
        </Form.Item>
        <Form.Item
          label="Họ và Tên"
          name="hoTen"
          rules={[
            {
              required: true,
              message: "Nhập Họ Tên",
            },
          ]}>
          <Input
            className="py-3 text-gray-800 placeholder:font-bold hover:border-blue-600"
            placeholder="nhập họ tên ..."
          />
        </Form.Item>
        <Form.Item
          label="Nhập email"
          name="email"
          rules={[
            {
              required: true,
              message: "Nhập email",
            },
          ]}>
          <Input
            className="py-3 text-gray-800 placeholder:font-bold hover:border-blue-600"
            placeholder="nhập email ..."
          />
        </Form.Item>
        <Form.Item
          label="Điền số điện thoại"
          name="soDt"
          rules={[
            {
              required: true,
              message: "Nhập số điện thoại",
            },
          ]}>
          <Input
            className="py-3 text-gray-800 placeholder:font-bold hover:border-blue-600"
            placeholder="nhập số điện thoại ..."
          />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            span: 24,
          }}>
          <Button
            style={{ lineHeight: "5px" }}
            className="block w-full py-6 mb-5 text-white bg-blue-500"
            type="primary"
            htmlType="submit">
            Đăng ký
          </Button>
          <NavLink
            style={{ textDecoration: "underline" }}
            to={"/login"}
            className="block text-right">
            <span className="text-purple-900">
              Bạn đã có tài khoản rồi. Nhấn vào đây để Đăng nhập.
            </span>
          </NavLink>
        </Form.Item>
      </Form>
    </>
  );
}

export default RegisterPage;
