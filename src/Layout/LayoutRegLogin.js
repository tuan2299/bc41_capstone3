import React from "react";
import Headers from "../Components/Header/Header";

function LayoutRegLogin({ Component }) {
  return (
    <div className="min-h-screen layout__reglogin">
      <Headers />
      <div className="flex items-center justify-center w-full h-full py-50">
        <Component />
      </div>
    </div>
  );
}

export default LayoutRegLogin;
