import React from "react";
import Headers from "../Components/Header/Header";

export default function LayoutHome({ Component }) {
  return (
    <div className="flex flex-col h-full min-h-screen">
      <Headers />
      {/* Bên trong component sẽ chứa danh sách phim , tab rạp phim */}
      <div className="flex-grow mt-5">
        <Component />
      </div>
    </div>
  );
}
