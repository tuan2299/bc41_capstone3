import DetailMovie from "../Components/DetailMovie/DetailMovie";
import LayoutHome from "../Layout/LayoutHome";
import LayoutRegLogin from "../Layout/LayoutRegLogin";
import DetailPage from "../Pages/DetailPage/DetailPage";
import HomePage from "../Pages/HomePage/HomePage";
import LoginPage from "../Pages/LoginPage/LoginPage";
import RegisterPage from "../Pages/Register/RegisterPage";

export const userRoute = [
  // * Đây là nơi chứa các thông tin của các route liên quan tới các trang page dành cho user
  {
    path: "/",
    component: <LayoutHome Component={HomePage} />,
  },
  {
    path: "/login",
    component: <LayoutRegLogin Component={LoginPage} />,
  },
  {
    path: "/register",
    component: <LayoutRegLogin Component={RegisterPage} />,
  },
  {
    path: "/detail/:id",
    component: <DetailPage Component={DetailMovie} />,
  },
];
