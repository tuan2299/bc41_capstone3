import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isOpen: false,
  url: null,
};

const modalSlice = createSlice({
  name: "modalSlice",
  initialState,
  reducers: {
    handleMoModal: (state, action) => {
      state.isOpen = true;
      state.url = action.payload;
    },
    handleTatModal: (state, action) => {
      state.isOpen = false;
      state.url = null;
    },
  },
});

export const { handleMoModal, handleTatModal } = modalSlice.actions;

export default modalSlice.reducer;
