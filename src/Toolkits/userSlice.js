import { createSlice } from "@reduxjs/toolkit";
import { localUserServ } from "../Services/localService";

const initialState = {
  // khởi tạo state ban đầu
  userInfo: localUserServ.get(),
};

const userSlice = createSlice({
  name: "userSlice",
  initialState,
  reducers: {
    // bên trong object reducers của slice sẽ liệt kê ra các action cần thiết
    setUserInfo: (state, action) => {
      state.userInfo = action.payload;
    },
    setLoginUser: (state, action) => {
      state.userInfo = action.payload;
    },
  },
});
// sau khi tạo ra các action từ đó bóc tách các action
export const { setUserInfo, setLoginUser } = userSlice.actions;

export default userSlice.reducer;
